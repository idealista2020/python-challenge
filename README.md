# Idealista/data python-challenge

El reto consiste en desarrollar un script que simule una navegación de usuario, se parte de un conjunto situaciones de navegación para las que se dispone de su probabilidad de ocurrencia. Se pretende conseguir un desarrollo eficiente en python que simule la navegación más probable desde un estado inicial hasta los siguientes 8 estados y que se capaz de hacerlo en un tiempo inferior a 5 minutos (el entorno de ejecución será un i7 con 4 procesadores y 32Gb de Ram).

## Secuencias de navegación

Una secuencia de navegación se compone de una lista de estados codificados como números, en este caso:

```
0,2,99
```

En este flujo de navegación la sesión se inicia con el estado 0, pasa al 2 y termina en el 99.

El modelo de navegación es un modelo probabilistico para el que se tiene una probabilidad de que suceda un cambio de estado. La simulación
debe estimar cuales son los flujos de navegacion más probables a partir de una secuencia, para ello se proporcionan una serie de transiciones entre secuencias de estados y un nuevo estado. Para cada transición se incluye una probabilidad de que suceda en el atributo **PROB**.
Como en el ejemplo siguiente dónde se indica cuál la probabilidad (en tanto por uno) de pasar del estado 5,5 al estado 5,5,7 es de 0.004089979550102249 .

```
PARENT;PATTERN;PROB
5,5;5,5,7;0.004089979550102249
```

El modelo de navegación a desarrollar debe ejecutarse de forma recursiva explorando las transiciones que se pueden producir en cada uno de los niveles, por ejemplo si disponemos solamente estas transiciones:

```
PARENT;PATTERN;PROB
1,1;1,1,0;0.004
1,0;1,0,1;0.014
1,0;1,0,0;0.003
0,0;0,0,1;0.010
0,0;0,0,1;0.003
0,1;0,1,1;0.012
0,1;0,0,0;0.007
```

Podríamos construir una serie de escenarios a partir de una navegación inicial "1,1", estos escenarios sería los siguientes (definido como un árbol de navegación) para una profundidad de navegación de 3:

```

Inicio - Nivel 0: cadena:"1,1" prob: 1
 Situación Nivel 1 - caso 1: cadena:"1,1,0" prob: 0.004 - Transition used: 1,1 -> 1,1,0
    Situación Nivel 2 - caso 1: cadena:"1,1,0,0" prob: 0.004 x 0.003 - Transition used: 1,0 -> 1,0,0
        Situación Nivel 3 - caso 1: cadena:"1,1,0,0,1" prob: 0.004 x 0.003 x 0.010 - Transition used: 0,0 -> 0,0,1
        Situación Nivel 3 - caso 1: cadena:"1,1,0,0,0" prob: 0.004 x 0.003 x 0.003 - Transition used: 0,0 -> 0,0,0
    Situación Nivel 2 - caso 1: cadena:"1,1,0,1" prob: 0.004 x 0.014  - Transition used: 1,0 -> 1,0,1
        Situación Nivel 3 - caso 1: cadena:"1,1,0,1,1" prob: 0.004 x 0.014 x 0.012 - Transition used: 0,1 -> 0,1,1
        Situación Nivel 3 - caso 1: cadena:"1,1,0,1,0" prob: 0.004 x 0.014 x 0.007 - Transition used: 0,1 -> 0,1,0

```

Como se puede ver la probabilidad final de cualquier estado se puede calcular como:

```
 Probabilidad estado n = Probabilidad estado n - 1 x Probabilidad transición usada
```

En el ejmplo en caso **Situación Nivel 3 - caso 1: cadena:"1,1,0,1,0" prob: 0.004 x 0.014 x 0.007 - Transition used: 0,1 -> 0,1,0** nos dice que desde la situación inicial "1,1" tenemos una
probabilidad de terminar en el estado "1,1,0,1,0" de  "0.004 x 0.014 x 0.007" = 0,000000392

# Cómo debe invocarse al proceso

El proceso se deberá llamar a través de un script llamado calculate-browsing-chain.py que tome dos parámetros como entrada:

* Cadena de navegacion actual que será una secuencia de estados identificados con números enteros
* Profundidad de la navegación, que será el número de niveles de profunidad a recorrer

# Ejemplo de ejecución y salida

El script debe invocarse así:

```
$ ./calculate-browsing-chain.py current-session="0,2" browsing-depth=10 output=../data/out/output.file.csv.gz
```

Cuyos parámetros serían:
* current-session : Estado inicial en formato cadena
* browsing-depth : Profundidad de navegación a simular
* output : Nombre del fichero donde vamos a cuardar el resultado en csv comprimido

La salida se guardaría en un fichero de salida (queda a criterio del desarrollador el formato a almacenar el fichero pero debe ser csv comprimido con gzip) y debe mostrar por consola los mensajes de la siguiente forma

```
Current Starting Session: 0,2

Depth=1 Current-Session: 0,2
 Most likely statuses (1/3): 0,2,99 prob=0.42
 Most likely statuses (2/3): 0,2,0 prob=0.15
 Most likely statuses (3/3): 0,2,1 prob=0.12

Depth=2
 Most likely statuses (1/3): 0,2,0,1 prob=0.12
 Most likely statuses (2/3): 0,2,0,99 prob=0.09
 Most likely statuses (3/3): 0,2,1,7 prob=0.06

....<resto de niveles del 3 al 9>

Depth=10
 Most likely statuses (1/3): 0,2,0,4,0,1,0,1,2,7,0,1 prob=0.19
 Most likely statuses (1/3): 0,2,0,7,3,1,0,1,4,1,0,1 prob=0.13
 Most likely statuses (1/3): 0,2,1,1,7,1,2,1,0,1,0,1 prob=0.06

```

# Datos de entrada

En la carpeta **data/in/** se encuentra el fichero de datos con las transiciones en entre estados, este fichero **model.transitions.csv.gz** contiene
una secuencia de estados con los campos con los que se tendrá que montar el modelo de simulación:

* **SOURCE** - Es el estado de inicio
* **DESTINATION** - Es el estado de destino
* **PROB** - Es la probabilidad de la transición

```
SOURCE;DESTINATION;PROB
0,2;0,2,7;7.870188190088194e-4
5,5;5,5,7;0.004089979550102249
6,6;6,6,-1;0.05000000000000001
9,5;9,5,0;0.4166666666666667
0,6;0,6,2;0.20954907161803712
1,7;1,7,0;0.10836277974087163
6,6;6,6,0;0.6
2,9;2,9,6;0.0012554927809165094
1,7;1,7,-1;0.0058892815076560644
```
